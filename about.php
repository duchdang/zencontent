<section id="full-width-slider">
    <div class="container">
        <?php get_sidebar(); ?>
        <div class="row banner-2">
            <div class="col-sm-12 col-md-12 text-center slider-content home-content">
                <div class="col-xs-12 col-sm-6">
                    <h2 class="text-right m-l-20"> About <span class="text-black hidden-xs">|</span></h2>
                </div>
                <div class="text-sperate hidden-sm hidden-md hidden-lg"></div>
                <div class="col-xs-12 col-sm-5 p-l-0">
                    <h2 class="text-left"><span class="text-type">People</span></h2>
                </div>
                
                <div class="clearfix"></div>
                <p class="text-center hidden-xs hidden-sm">ZenContent provides quality content creation for your
                    business. With our team of professional writers and editors, we deliver subject matter expertise to
                    your specialization. Working with you we’ll manage the entire content creation lifecycle, so you can
                    focus on your business. We like to think that our content can help you grow.</p>
                <a class="btn btn-black hidden-sm" href="<?php echo site_url(); ?>/contact">TALK TO US</a>
            </div>
        </div>
    </div>
</section>

<section id="content">
<div class="container" id="background">
    <div class="row hidden-xs">
        <nav id="scroll-menu" class="navbar navbar-inverse navbar-static no-border-radius" data-offset-top="660" data-spy="affix">
            <div class="col-xs-12 text-center hidden-lg hidden-md hidden-about-title">About ZenContent</div>
            <div class="container-fluid">
                <div class="navbar-header m-r-20">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand hidden-xs hidden-sm" href="#">About ZenContent</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#background">Background</a></li>
                        <li><a href="#team">Team</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>

                    <div class="navbar-form navbar-right">
                        <a href="<?php echo site_url(); ?>/writer" class="btn btn-dark-blue">JOIN ZENCONTENT</a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <div class="row">
        <div class="col-xs-12 p-80 p-t-20 p-b-40">
            <h2 class="text-dark-blue m-b-20">Decades of experience, millions of words.</h2>

            <p>The founders of ZenContent™ have years of experience wading through the often-stressful waters of 
            enterprise content creation. For the last decade, we oversaw an agency that produced millions of words 
            woven into share-worthy content for some of the biggest brands on the Web.</p>

            <p>As we established and grew the well-equipped community we have today, we built a foundation for 
            creating high quality, original content. Using a distributed workforce, some automation, and a few 
            handy-dandy complex algorithms (don't worry, we handle those), we devised a proven method of content 
            creation that works for any brand. Everything felt Zen.</p>

            <p>How did we do it? Our agency designed and tested a very efficient system that scores the content 
            created by our work-from-home professional creators. Generating hundreds of thousands of unique, 
            original pieces that our clients loved, we tested this system from start to finish and perfected it 
            along the way. Even better, we found it simple to scale as our agency grew, highlighting the system's 
            flexibility for businesses of all sizes. We knew we had to share it with the world.</p>

            <p>That's why we're opening up our platform to individuals, SMBs, and large brands who want to access 
            this tried-and-true system without worrying about the details. ZenContent™ is here to help make any site 
            shine, offering features that set us apart from similar content connoisseurs. Keep an eye on our journal 
            for news about exciting new features, and to watch us grow even more.</p>
        </div>
    </div>
    <div class="row type-2" id="team">
        <div class="col-xs-12 p-40 p-b-30">
            <div class="sec-title">
                <h3>Our team</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row hidden-xs p-40 p-t-0">
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default no-border-radius post team">
                    <div class="post-image">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-1.jpg" alt=""/>
                    </div>
                    <div class="post-info">
                        <a href="https://www.linkedin.com/in/briannademike" target="_blank"><img
                                src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                        <h6>Brianna DeMike<br>Operations</h6>

                        <p>Brianna is at the forefront ZenContent's executive team, overseeing vital operations that
                            include marketing, product development, workforce management, and engineering.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default no-border-radius post team">
                    <div class="post-image">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-2.jpg" alt=""/>
                    </div>
                    <div class="post-info">
                        <a href="http://www.linkedin.com/in/kimamccarthy" target="_blank"><img
                                src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                        <h6>Kimberly Bubeck<br>Account & Project Management</h6>

                        <p>Leading ZenContent's account and project management teams, Kimberly combines innovative
                            methods that emphasize her passion for process control and meeting deadlines.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default no-border-radius post team">
                    <div class="post-image">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-3.jpg" alt=""/>
                    </div>
                    <div class="post-info">
                        <a href="https://www.linkedin.com/in/curranrebecca" target="_blank"><img
                                src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                        <h6>Becky Curran<br>Enterprise Account Management</h6>

                        <p>Directing ZenContent's workforce management team, Becky oversees the fulfillment of client
                            projects from start to finish. Her passion for people and drive for success fuels her
                            enthusiasm for leading large teams.</p>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default no-border-radius post team">
                    <div class="post-image">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-4.jpg" alt=""/>
                    </div>
                    <div class="post-info">
                        <a href="https://www.linkedin.com/pub/sage-curtis/70/38b/501" target="_blank"><img
                                src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                        <h6>Sage Curtis<br>Marketing</h6>

                        <p>Sage is passionate and energetic about targeted lead generation. She works with sales teams
                            to deliver revenue, disrupt markets and build brand awareness through messaging,
                            positioning, PR, social media, and tactical marketing campaigns.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default no-border-radius post team">
                    <div class="post-image">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-5.jpg" alt=""/>
                    </div>
                    <div class="post-info">
                        <a href="https://www.linkedin.com/pub/michelle-watson/9/671/66" target="_blank"><img
                                src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                        <h6>Michelle Watson<br>Editorial Management</h6>

                        <p>As the overseer of the ZenContent voice, Michelle unifies vast knowledge of the management
                            trade with a comprehensive understanding of our clients.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default no-border-radius post team">
                    <div class="post-image">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-6.jpg" alt=""/>
                    </div>
                    <div class="post-info">
                        <a href="https://www.linkedin.com/pub/kayci-webster/76/218/267" target="_blank"><img
                                src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                        <h6>Kayci Webster<br>Community Management</h6>

                        <p>ZenContent's Community Management team is the voice of the company and its many customers. As
                            the head of this valuable team, Kayci personally connects with creators to increase user
                            retention, build product loyalty, and provide feedback to internal departments.</p>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default no-border-radius post team">
                    <div class="post-image">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-7.jpg" alt=""/>
                    </div>
                    <div class="post-info">
                        <a href="https://www.linkedin.com/in/sirjustindart" target="_blank"><img
                                src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                        <h6 style="margin:-4px 0px 4px">Justin Dart<br>Business Development & Partnerships</h6>

                        <p>Justin's work with ZenContent is fundamental to sales team growth, productivity improvement,
                            and channel development. He is passionate about helping others reach their fullest potential
                            to uphold client success.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default no-border-radius post team">
                    <div class="post-image">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-8.jpg" alt=""/>
                    </div>
                    <div class="post-info">
                        <a href="https://www.linkedin.com/pub/jose-perez/49/757/341" target="_blank"><img
                                src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                        <h6>Jose Perez<br>Engineering</h6>

                        <p>Jose leads the ZenContent team responsible for building crucial tools and platforms that
                            support engineering operations, including our proprietary quality scoring and pairing
                            software.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default no-border-radius post team">
                    <div class="post-image">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-9.jpg" alt=""/>
                    </div>
                    <div class="post-info">
                        <a href="https://www.linkedin.com/in/meaghanmcbee" target="_blank"><img
                                src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                        <h6>Meaghan McBee<br>Content Strategy</h6>

                        <p>As an avid content strategist, Meaghan is a creative thinker tasked with shaping the future
                            of ZenContent brands through ingenuity and team collaboration.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row hidden-sm hidden-md hidden-lg p-40 p-t-0">
            <div id="list-team">
                <div class="col-xs-12 item">
                    <div class="panel panel-default no-border-radius post team">
                        <div class="post-image">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-1.jpg" alt=""/>
                        </div>
                        <div class="post-info">
                            <a href="https://www.linkedin.com/in/briannademike" target="_blank"><img
                                    src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                            <h6>Brianna DeMike<br>Operations</h6>

                            <p>Brianna is at the forefront ZenContent's executive team, overseeing vital operations that
                                include marketing, product development, workforce management, and engineering.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 item">
                    <div class="panel panel-default no-border-radius post team">
                        <div class="post-image">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-2.jpg" alt=""/>
                        </div>
                        <div class="post-info">
                            <a href="http://www.linkedin.com/in/kimamccarthy" target="_blank"><img
                                    src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                            <h6>Kimberly Bubeck<br>Account & Project Management</h6>

                            <p>Leading ZenContent's account and project management teams, Kimberly combines innovative
                                methods that emphasize her passion for process control and meeting deadlines.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 item">
                    <div class="panel panel-default no-border-radius post team">
                        <div class="post-image">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-3.jpg" alt=""/>
                        </div>
                        <div class="post-info">
                            <a href="https://www.linkedin.com/in/curranrebecca" target="_blank"><img
                                    src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                            <h6>Becky Curran<br>Enterprise Account Management</h6>

                            <p>Directing ZenContent's workforce management team, Becky oversees the fulfillment of client
                                projects from start to finish. Her passion for people and drive for success fuels her
                                enthusiasm for leading large teams.</p>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 item">
                    <div class="panel panel-default no-border-radius post team">
                        <div class="post-image">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-4.jpg" alt=""/>
                        </div>
                        <div class="post-info">
                            <a href="https://www.linkedin.com/pub/sage-curtis/70/38b/501" target="_blank"><img
                                    src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                            <h6>Sage Curtis<br>Marketing</h6>

                            <p>Sage is passionate and energetic about targeted lead generation. She works with sales teams
                                to deliver revenue, disrupt markets and build brand awareness through messaging,
                                positioning, PR, social media, and tactical marketing campaigns.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 item">
                    <div class="panel panel-default no-border-radius post team">
                        <div class="post-image">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-5.jpg" alt=""/>
                        </div>
                        <div class="post-info">
                            <a href="https://www.linkedin.com/pub/michelle-watson/9/671/66" target="_blank"><img
                                    src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                            <h6>Michelle Watson<br>Editorial Management</h6>

                            <p>As the overseer of the ZenContent voice, Michelle unifies vast knowledge of the management
                                trade with a comprehensive understanding of our clients.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 item">
                    <div class="panel panel-default no-border-radius post team">
                        <div class="post-image">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-6.jpg" alt=""/>
                        </div>
                        <div class="post-info">
                            <a href="https://www.linkedin.com/pub/kayci-webster/76/218/267" target="_blank"><img
                                    src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                            <h6>Kayci Webster<br>Community Management</h6>

                            <p>ZenContent's Community Management team is the voice of the company and its many customers. As
                                the head of this valuable team, Kayci personally connects with creators to increase user
                                retention, build product loyalty, and provide feedback to internal departments.</p>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 item">
                    <div class="panel panel-default no-border-radius post team">
                        <div class="post-image">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-7.jpg" alt=""/>
                        </div>
                        <div class="post-info">
                            <a href="https://www.linkedin.com/in/sirjustindart" target="_blank"><img
                                    src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                            <h6 style="margin:-4px 0px 4px">Justin Dart<br>Business Development & Partnerships</h6>

                            <p>Justin's work with ZenContent is fundamental to sales team growth, productivity improvement,
                                and channel development. He is passionate about helping others reach their fullest potential
                                to uphold client success.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 item">
                    <div class="panel panel-default no-border-radius post team">
                        <div class="post-image">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-8.jpg" alt=""/>
                        </div>
                        <div class="post-info">
                            <a href="https://www.linkedin.com/pub/jose-perez/49/757/341" target="_blank"><img
                                    src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                            <h6>Jose Perez<br>Engineering</h6>

                            <p>Jose leads the ZenContent team responsible for building crucial tools and platforms that
                                support engineering operations, including our proprietary quality scoring and pairing
                                software.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 item">
                    <div class="panel panel-default no-border-radius post team">
                        <div class="post-image">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/team/about-team-9.jpg" alt=""/>
                        </div>
                        <div class="post-info">
                            <a href="https://www.linkedin.com/in/meaghanmcbee" target="_blank"><img
                                    src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-in.png" alt="" class="ico-info"/></a>
                            <h6>Meaghan McBee<br>Content Strategy</h6>

                            <p>As an avid content strategist, Meaghan is a creative thinker tasked with shaping the future
                                of ZenContent brands through ingenuity and team collaboration.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="clearfix"></div>
    </div>
    <div class="row type-4" id="contact">
        <div class="col-xs-12 p-40 p-b-30">
            <div class="sec-title">
                <h3>Get in touch</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row p-80 p-t-0 about-contact">
            <div class="col-xs-12 col-sm-7 col-md-6">
                <iframe id="about-gg-map"
                        frameborder="0"
                        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDDrA4gKvpo35LLeqIKuueaVmkawE2LQ40
                                    &q=5201+Great+America+Pkwy,+Santa+Clara,+CA+95054" allowfullscreen>
                </iframe>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 p-l-30">
                <h3 class="m-t-0">Santa Clara, California</h3>
                <p>ZenContent, Inc.<br>5201 Great America Parkway, Suite 320<br>Santa Clara, CA 95054</p>
                <h3>General Inquiries</h3>
                <p class="text-pink-red">
                    +1 (855) 874-2878<br>
                    <a class="text-pink-red" href="mailto:hello@zencontent.com">hello@zencontent.com</a>
                </p>
                <h3>Support</h3>
                <p class="text-pink-red">
                    +1 (855) 874-2878<br>
                    <a class="text-pink-red" href="mailto:hello@zencontent.com">hello@zencontent.com</a>
                </p>
            </div>
        </div>
    </div>
    <div class="row type-3 p-b-30 p-t-15 text-center" id="join">
        <h2 class="p-l-20 p-r-20 m-t-30 text-light-blue">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/leaf.jpg" alt="">
            Be a part of revolutionary writing.
        </h2>
        <a class="btn btn-black m-t-00 m-b-15" href="<?php echo site_url(); ?>/writer">JOIN ZENCONTENT</a>
    </div>
    <div class="row p-35 text-center div-relax">
        <h2 class="text-dark-blue m-0">Relax, we’ve got this.™</h2>
    </div>
</div>
</section>
<script type="text/javascript">
    var page = 'about';
</script>