<?php
//wp_redirect(site_url().'/about-us' );
//exit;
?>

<?php get_header(); ?>
<section id="full-width-slider">
    <div class="container">
        <?php get_sidebar(); ?>
        <div class="row">
            <div id="video-container" class="hidden-xs">
                <video autoplay loop class="fillWidth">
                    <source src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/video/home-video.mp4" type="video/mp4"/>
                    <source src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/video/home-video.ogv" type="video/ogg"/>
                    <source src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/video/home-video.webm" type="video/webm"/>
                    Your browser does not support the video tag. I suggest you upgrade your browser.
                </video>
            </div>
            <div class="col-sm-12 col-md-12 text-center slider-content home-content">
                <div class="col-xs-12 col-sm-6">
                    <h2 class="text-right m-l-20"> Create <span class="text-black hidden-xs">|</span></h2>
                </div>
                <div class="text-sperate hidden-sm hidden-md hidden-lg"></div>
                <div class="col-xs-12 col-sm-5 p-l-0">
                    <h2 class="text-left"><span class="text-type">Interest</span></h2>
                </div>
                <div class="clearfix"></div>

                <p class="text-center hidden-xs">At ZenContent, we keep it simple and make it great. Each talented
                 team provides quality content to help your business thrive while you focus on what matters most. 
                 We work with you every step of the way, managing the entire process from design to delivery. 
                 While you kick back and relax, expert creators take over to cultivate content that fits seamlessly
                 with your business. Let's make your site a success.</p>
                <a class="btn btn-black" href="<?php echo site_url(); ?>/contact">TALK TO US</a>
            </div>
        </div>
    </div>
</section>

<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 p-40 p-t-10">
                <div class="sec-title">
                    <h4>How we make</h4>

                    <h3>your content shine</h3>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 text-center">
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/define.jpg" alt="">

                <h3>Define</h3>

                <p class="p-20">Dedicated talented teams work closely with you to outline and define your needs.</p>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 text-center">
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/caollaborate.jpg" alt="">

                <h3>Collaborate</h3>

                <p class="p-20">Creators are invited to collaborate on your content, or we assign them based on expertise.</p>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 text-center">
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/deliver.jpg" alt="">

                <h3>Deliver</h3>

                <p class="p-20">Your content is reviewed, approved, and delivered right to you.</p>
            </div>
        </div>
        <div class="row type-2 p-b-35">
            <div class="col-xs-12 p-40 p-t-15 p-b-10">
                <div class="sec-title">
                    <h4>When you're ready to relax,</h4>

                    <h3>we're ready to work</h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row p-40 p-t-0 p-b-20">
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="panel panel-default home-panel red">
                        <div class="panel-body">
                            <div class="col-xs-6 col-xs-offset-6">
                                <ul>
                                    <li><a href="#">Tweets</a></li>
                                    <li><a href="#">Queries</a></li>
                                    <li><a href="#">Answers</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <h3>Short Form</h3>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="panel panel-default home-panel blue">
                        <div class="panel-body">
                            <div class="col-xs-6 col-xs-offset-6">
                                <ul>
                                    <li><a href="#">Articles</a></li>
                                    <li><a href="#">Reviews</a></li>
                                    <li><a href="#">Blog posts</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <h3>Long Form</h3>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="panel panel-default home-panel yellow">
                        <div class="panel-body">
                            <div class="col-xs-7 col-xs-offset-5">
                                <ul>
                                    <li><a href="#">White papers</a></li>
                                    <li><a href="#">Product descriptions</a></li>
                                    <li><a href="#">Buying guides</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <h3>Technical</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <a class="btn btn-black" href="<?php echo site_url(); ?>/contact">TALK TO US</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row p-b-15">
            <div class="col-xs-12 p-40 p-t-10 p-b-15">
                <div class="sec-title">
                    <h4>Companies that like content,</h4>
                    <h3>love ZenContent</h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row p-40 p-t-0 p-b-20">
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="panel panel-default no-border-radius post">
                        <div class="post-image">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/news-1.jpg" alt=""/>
                        </div>
                        <div class="post-space">
                        </div>
                        <div class="post-info">
                            <h6>Ask.com
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-info.png" alt="" class="ico-info"/>
                            </h6>
                            <p>"When it comes to partners, we look for transparency. We don’t want to work with a blackbox company. We work closely with ZenContent to ensure writers get paid fairly so that we have a reliable stream of quality content."</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="panel panel-default no-border-radius post">
                        <div class="post-image">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/news-2.jpg" alt=""/>
                        </div>
                        <div class="post-space">
                        </div>
                        <div class="post-info">
                            <h6>Homerun Creative
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-info.png" alt="" class="ico-info"/>
                            </h6>
                            <p>"As a marketing agency, ZenContent makes it easy to manage several client projects at once. It's fully-managed, easy-to-use platform connects our content assignments with subject matter experts who meet the needs of every customer vertical. “</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="panel panel-default no-border-radius post">
                        <div class="post-image">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/news-3.jpg" alt=""/>
                        </div>
                        <div class="post-space">
                        </div>
                        <div class="post-info">
                            <h6>Coopervision
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ico-info.png" alt="" class="ico-info"/>
                            </h6>
                            <p>"When our team is getting stretched, we need access to a skilled force to help us address our capacity when we see spikes in business. ZenContent has been a great partner to help us grow our business without worrying about the cost of ramping up staff internally.”</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row type-2">
            <div class="row wrapper-list-logo">
                <?php echo do_shortcode('[jw_easy_logo slider_name="Logo"]'); ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row type-3 p-b-40 p-t-15 text-center">
            <h2 class="m-t-40">Ready to create content with ZenContent?</h2>
            <a class="btn btn-black m-t-15 m-b-15" href="<?php echo site_url(); ?>/contact">TALK TO US</a>
            <h4>OR CALL <span class="text-yellow">1 (855) 874-2878</span></h4>
        </div>
        <div class="row type-4 p-b-35 hidden">
            <div class="col-xs-12 p-40 p-t-15 p-b-10">
                <div class="sec-title">
                    <h4>Insight and articles,</h4>
                    <h3>from our Blog</h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row p-40 p-t-0 p-b-20">
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="panel panel-yellow">
                        <div class="panel-body">
                            <h3>Nulla vitae elit libero, a pharetra augue.</h3>
                            <p>ZenContent</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="panel panel-yellow">
                        <div class="panel-body">
                            <h3>Ut fermentum massa justo sit amet risus.</h3>
                            <p>ZenContent</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="panel panel-yellow">
                        <div class="panel-body">
                            <h3>Fusce dapibus, tellus ac cursus commodo, tortor mauris.</h3>
                            <p>ZenContent</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12">
                    <a href="#" class="home-view-all-post">View all post</a>
                </div>
            </div>
        </div>
        <div class="row p-35 text-center div-relax">
            <h2 class="text-dark-blue m-0">Relax, we’ve got this.™</h2>
        </div>
    </div>
</section>
<script type="text/javascript">
    var page = 'home';
</script>
<?php get_footer(); ?>
